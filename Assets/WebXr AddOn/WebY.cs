﻿using Zinnia.Action;
using WebXR;
using UnityEngine;

public class WebY : FloatAction
{
    [SerializeField] private WebXRController controller;
    private float yAxis;

    // Update is called once per frame
    void Update()
    {
        var vector = controller.GetAxis("Vertical");
        yAxis = vector;
        Receive(yAxis);
    }
}
