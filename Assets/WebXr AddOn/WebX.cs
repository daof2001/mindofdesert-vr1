﻿using Zinnia.Action;
using WebXR;
using UnityEngine;

public class WebX : FloatAction
{
    [SerializeField] private WebXRController controller;
    private float xAxis;

    // Update is called once per frame
    void Update()
    {
        var vector = controller.GetAxis("Horizontal");
        xAxis = vector;
        Receive(xAxis);
    }
}
