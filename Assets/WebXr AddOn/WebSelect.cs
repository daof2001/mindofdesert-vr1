﻿using Zinnia.Action;
using WebXR;
using UnityEngine;

public class WebSelect : BooleanAction
{
    [SerializeField] private WebXRController controller;

    // Update is called once per frame
    void Update()
    {
        Receive(controller.GetButton("Trigger"));
    }
}
