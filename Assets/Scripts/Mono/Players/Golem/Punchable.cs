using UnityEngine;

namespace Mono.Players.Golem
{
    [RequireComponent(typeof(Animator))]
    public class Punchable : MonoBehaviour
    { 
        private Animator _animator;
    
        // Start is called before the first frame update
        void Start()
        {
            _animator = GetComponent<Animator>();
        }
    
        public void TriggerPunchBehaviour()
        {
            _animator.SetBool("Punched", true);
        }
    }
}
