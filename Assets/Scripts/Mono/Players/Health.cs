using System;
using UnityEngine;
using UnityEngine.UI;

namespace Mono.Players
{
    public class Health : MonoBehaviour
    {
        private float _defaultHealthPoints;
        
        [SerializeField] private Slider healthBar;
        
        [SerializeField] private float health;
        
        [Range(0,100)]
        [SerializeField] private float poisonDamageRate;
    
        // Start is called before the first frame update
        private void Start()
        {
            _defaultHealthPoints = health;
        }

        // Update is called once per frame
        private void Update()
        {
            if (health <= 0)
            {
                GameManager.GameManager.Instance.RespawnPlayers();
                health = _defaultHealthPoints;
            }

            healthBar.value = health / _defaultHealthPoints;
        }

        public void DamagePlayer(float damage)
        {
            health -= damage;
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("Poison"))
            {
                health -= poisonDamageRate * Time.deltaTime;
            }
        }
    }
}
