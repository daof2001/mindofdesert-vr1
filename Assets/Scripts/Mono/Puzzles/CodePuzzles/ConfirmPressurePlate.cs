using System;
using DG.Tweening;
using UnityEngine;

namespace Mono.Puzzles.CodePuzzles
{
    public class ConfirmPressurePlate : MonoBehaviour,IPressurePlate
    {
        public static Action ResetPlates;
        
        [SerializeField] private string correctCode;
        [SerializeField] private Door[] doors;

        private string _currentCode;

        private bool _isCodeRight;
        private bool _isPressed;
        private float _initialPosY;

        private void Start()
        {
            _initialPosY = transform.position.y;
        }

        private void TryDoors()
        {
            if (!_isCodeRight)
            {
                _currentCode = "";
                ResetPlates?.Invoke();
                return;
            }
            
            Debug.Log("Code Right");

            if (doors.Length == 0) return;

            foreach (var door in doors)
            {
                door.OpenDoor();
            }
        }

        private void AddCharToCode(char input)
        {
            Debug.Log($"Received {input}");
            
            _currentCode += input;

            if (input != correctCode[_currentCode.Length - 1])
            {
                _currentCode = "";
                ResetPlates?.Invoke();
                return;
            }

            _isCodeRight = _currentCode.Equals(correctCode);
        }

        public void OnPressurePlatePressed()
        {
            transform.DOMoveY(_initialPosY - 0.15f, 1);
            TryDoors();
        }

        public void OnPressurePlateRaised()
        {
            transform.DOMoveY(_initialPosY, 1);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && !_isPressed )
            {
                _isPressed = true;
                OnPressurePlatePressed();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player") && _isPressed)
            {
                _isPressed = false;
                OnPressurePlateRaised();
            }
        }

        private void OnEnable()
        {
            CodePressurePlate.PressurePlatePressed += AddCharToCode;
        }

        private void OnDisable()
        {
            CodePressurePlate.PressurePlatePressed -= AddCharToCode;
        }
    }
}
