﻿using System;
using DG.Tweening;
using UnityEngine;

namespace Mono.Puzzles.CodePuzzles
{
    public class CodePressurePlate : MonoBehaviour, IPressurePlate
    {
        public static Action<char> PressurePlatePressed;
        
        [SerializeField] private char codeInput;

        private bool _wasPressed = false;
        private float _raisedPosY;
        private AudioSource _source;

        private void Start()
        {
            _raisedPosY = transform.position.y;
            _source = GetComponent<AudioSource>();
        }

        public void OnPressurePlatePressed()
        {
            if (_wasPressed == false)
            {
                transform.DOMoveY(_raisedPosY - 0.15f, 1);
                _source.Play();
                
                PressurePlatePressed?.Invoke(codeInput);
                _wasPressed = true;
            }
        }

        private void RaisePlate()
        {
            if (_wasPressed)
            {
                transform.DOMoveY(_raisedPosY, 1);
                _wasPressed = false;
            }
            
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && !_wasPressed)
            {
                OnPressurePlatePressed();
            }
        }

        private void OnEnable()
        {
            ConfirmPressurePlate.ResetPlates += RaisePlate;
        }

        private void OnDisable()
        {
            ConfirmPressurePlate.ResetPlates -= RaisePlate;
        }
    }
}