﻿using UnityEngine;

namespace Mono.Puzzles.CodePuzzles
{
    public interface IPressurePlate
    {
        void OnPressurePlatePressed();
        
        //public void OnPressurePlateRaised();
    }
}