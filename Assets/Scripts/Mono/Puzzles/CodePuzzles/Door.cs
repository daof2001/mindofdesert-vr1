﻿using System;
using UnityEngine;

namespace Mono.Puzzles.CodePuzzles
{
    [RequireComponent(typeof(Animator))]
    public class Door : MonoBehaviour
    {
        private Animator _doorAnimator;

        private void Start()
        {
            _doorAnimator = GetComponent<Animator>();
        }

        public void OpenDoor()
        {
            _doorAnimator.SetBool("Open", true);
            GetComponent<AudioSource>().Play();
        }
    }
}