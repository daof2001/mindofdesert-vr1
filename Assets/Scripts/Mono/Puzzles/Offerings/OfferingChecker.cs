using System;
using System.Collections.Generic;
using System.Linq;
using Mono.Puzzles.CodePuzzles;
using UnityEngine;

namespace Mono.Puzzles.Offerings
{
    public class OfferingChecker : MonoBehaviour
    {
        private int _correctlyPlacedOfferings = 0;
        
        [SerializeField] private List<OfferingHolder> holders = new List<OfferingHolder>();
        [SerializeField] private Door door;
        
        void Start()
        {
           
        }
        private void CheckOfferings()
        {
            _correctlyPlacedOfferings = 0;
            
            foreach (var holder in holders.Where(holder => holder.CorrectOfferingPlaced))
            {
                _correctlyPlacedOfferings++;
            }

            if (_correctlyPlacedOfferings == holders.Count)
            {
                door.OpenDoor();
            }
        }

        private void OnEnable()
        {
            OfferingHolder.CheckOffering += CheckOfferings;
        }

        private void OnDisable()
        {
            OfferingHolder.CheckOffering -= CheckOfferings;
        }
    }
}
