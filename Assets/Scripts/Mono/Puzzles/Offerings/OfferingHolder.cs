using System;
using TMPro;
using UnityEngine;

namespace Mono.Puzzles.Offerings
{
    public class OfferingHolder : MonoBehaviour
    {
        public static Action CheckOffering;
        
        private bool _isCorrectOfferingPlaced;
        private bool _isOfferingPlaced;
        private Offering _currentOffering;
        
        [Header("Offering Settings")]
        [SerializeField] private Offering correctOffering;
        [SerializeField] private Transform offeringPos;

        public bool CorrectOfferingPlaced => _isCorrectOfferingPlaced;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.GetComponent<Offering>())
            {
                var o = other.gameObject;

                if (_currentOffering != null && _currentOffering != o.GetComponent<Offering>())
                {
                    _currentOffering.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                    _currentOffering.ReturnToSpawn();
                }
                
                o.transform.position = offeringPos.position;
                o.transform.parent = gameObject.transform;
                o.GetComponent<Rigidbody>().isKinematic = true;
                _currentOffering = other.gameObject.GetComponent<Offering>();
                
                _isCorrectOfferingPlaced = _currentOffering == correctOffering;
            
                CheckOffering?.Invoke();
            }
        }
    }
}
