using UnityEngine;

namespace Mono.Puzzles.Offerings
{
    public class Offering : MonoBehaviour
    {
        private Vector3 _initialPos;
        
        // Start is called before the first frame update
        void Start()
        {
            _initialPos = transform.position;
        }

        public void ReturnToSpawn()
        {
            transform.position = _initialPos;
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
