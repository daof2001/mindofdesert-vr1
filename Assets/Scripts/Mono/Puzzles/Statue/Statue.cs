using Mono.Puzzles.CodePuzzles;
using UnityEngine;

namespace Mono.Puzzles.Statue
{
    public class Statue : MonoBehaviour
    {
        [SerializeField] private GameObject firstPiece;
        [SerializeField] private GameObject secondPiece;
        [SerializeField] private GameObject statue;

        [SerializeField] private Transform firstPiecePos;

        [SerializeField] private Door _doorToOpen;

        private bool _isFirstPieceInPlace = false;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject == firstPiece)
            {
                firstPiece.transform.position = firstPiecePos.position;
                firstPiece.transform.rotation = firstPiecePos.rotation;
                Destroy(firstPiece.GetComponent<Collider>());
                Destroy(firstPiece.GetComponent<Rigidbody>());
                
                _isFirstPieceInPlace = true;
            }

            if (other.gameObject == secondPiece && _isFirstPieceInPlace)
            {
                Destroy(firstPiece);
                Destroy(secondPiece);
                Instantiate(statue, firstPiecePos.position, Quaternion.identity);
                GetComponent<AudioSource>().Play();
                _doorToOpen.OpenDoor();
            }
        }
    }
}
