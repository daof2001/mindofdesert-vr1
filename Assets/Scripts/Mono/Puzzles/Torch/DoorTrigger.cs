using System;
using Mono.Puzzles.CodePuzzles;
using UnityEngine;

namespace Mono.Puzzles.Torch
{
    public class DoorTrigger : MonoBehaviour
    {
        [SerializeField] private Door door;
        [SerializeField] private Transform boxPlace;
        [SerializeField] private GameObject correctBox;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject == correctBox)
            {
                if (boxPlace != null)
                {
                    other.transform.position = boxPlace.position;
                    other.transform.rotation = boxPlace.rotation;
                    other.transform.parent = boxPlace;
                    
                    if (other.TryGetComponent(out Rigidbody rb))
                        Destroy(rb);
                }
                    
                
                door.OpenDoor();
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(transform.position, door.transform.position);
        }
    }
}
