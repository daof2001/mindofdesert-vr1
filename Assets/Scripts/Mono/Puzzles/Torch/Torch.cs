using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Mono.Puzzles.Torch
{
    public class Torch : MonoBehaviour
    {
        private bool _wasExtinguished = false;

        private AudioSource _source;
        
        [SerializeField] private AudioClip extinguish;
        [SerializeField] private GameObject symbolToReveal;
        [SerializeField] private GameObject flameVFX;

        private void Start()
        {
            _source = GetComponent<AudioSource>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Blob") && !_wasExtinguished)
            {
                _wasExtinguished = true;
                _source.loop = false;
                _source.PlayOneShot(extinguish);
                flameVFX.SetActive(false);

                if (symbolToReveal != null)
                {
                    symbolToReveal.SetActive(true);
                }
                
            }
            
        }
    }
}
