using System;
//using Mono.Camera;
using Mono.CheckPoints;
using UnityEngine;
using UnityEngine.UI;

namespace Mono.GameManager
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        private CheckPoint _currentCheckPoint;
        
        private GameObject _blob;
        private GameObject _golem;
        private GameObject _blolem;
        /*
        private GameObject _blobCamTarget;
        private GameObject _golemCamTarget;
        */
        private bool _isBlolemActive = false;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
        }

        private void Update()
        {
            if (_blolem == null)
            {
                _isBlolemActive = false;
            }

            if (_blob.transform.position.y < -10 || _golem.transform.position.y < -10 && !_isBlolemActive)
            {
                RespawnPlayers();
            }
            
            if (_isBlolemActive)
            {
                if (_blolem.transform.position.y < -10)
                {
                    RespawnPlayers();
                }
            }
        }

        public void AssignBlob(GameObject blob, Transform camTarget)
        {
            _blob = blob;
           // _blobCamTarget = camTarget.gameObject;
        }

        public void AssignGolem(GameObject golem, Transform camTarget)
        {
            _golem = golem;
            //_golemCamTarget = camTarget.gameObject;
        }

        public void AssignBlolem(GameObject blolem)
        {
            _blolem = blolem;
            _isBlolemActive = true;
        }

        public bool CheckBlolemStatus()
        {
            return _isBlolemActive;
        }

        public void EnableBlob(Vector3 position)
        {
            _blob.transform.position = position;
            _blob.SetActive(true);
            //CameraController.Instance.SetCameraFocus(_blobCamTarget);
            //CameraController.Instance.SetBlobCamTarget(_blobCamTarget.transform);
        }

        public void EnableGolem(Vector3 position)
        {
            _golem.transform.position = position;
            _golem.SetActive(true);
        }

        public void SetCurrentCheckPoint(CheckPoint checkPoint)
        {
            _currentCheckPoint = checkPoint;
            Debug.Log("Checkpoint has been set");
        }

        public void RespawnPlayers()
        {
            if (_isBlolemActive)
            {
                _blolem.GetComponent<CharacterController>().enabled = false;
                _blolem.transform.position = _currentCheckPoint.transform.position;
                _blolem.GetComponent<CharacterController>().enabled = true;
                return;
            }
            
            _blob.GetComponent<CharacterController>().enabled = false;
            _blob.transform.position = _currentCheckPoint.BlobSpawnPos.position;
            _blob.GetComponent<CharacterController>().enabled = true;
            
            _golem.GetComponent<CharacterController>().enabled = false;
            _golem.transform.position = _currentCheckPoint.GolemSpawnPos.position;
            _golem.GetComponent<CharacterController>().enabled = true;
        }

        public GameObject GetBlob()
        {
            return _blob;
        }

        public GameObject GetGolem()
        {
            return _golem;
        }
    }
}
