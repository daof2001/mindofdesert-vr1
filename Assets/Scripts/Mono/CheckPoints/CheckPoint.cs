using System;
using UnityEngine;

namespace Mono.CheckPoints
{
    [RequireComponent(typeof(Rigidbody), typeof(BoxCollider))]
    public class CheckPoint : MonoBehaviour
    {
        private bool _hasBlobPassed = false;
        private bool _hasGolemPassed = false;

        private Rigidbody _myRb;

        [SerializeField] private Transform blobSpawnPos;
        [SerializeField] private Transform golemSpawnPos;

        public Transform BlobSpawnPos => blobSpawnPos;
        public Transform GolemSpawnPos => golemSpawnPos;
        
        // Start is called before the first frame update
        void Start()
        {
            _myRb = GetComponent<Rigidbody>();

            _myRb.isKinematic = true;
            _myRb.useGravity = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Blob"))
            {
                _hasBlobPassed = true;
            }

            if (other.CompareTag("Golem"))
            {
                _hasGolemPassed = true;
                
            }
            
            if (_hasBlobPassed && _hasGolemPassed)
            {
                GameManager.GameManager.Instance.SetCurrentCheckPoint(this);
                GetComponent<BoxCollider>().enabled = false;
            }
        }
    }
}
